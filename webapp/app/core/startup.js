/*global define*/
define([
  'zepto',
  'lodash',
  'collections/levels',
  'collections/games',
  'common'
], function ($, _, Levels, Games, Common) {
  'use strict';

  return function()
  {
    // First Launch
    Levels.create({
      type: 'schoolboy',
      title: 'Schoolboy',
      subtitle: '2 + 3',
      next_levels: ['student'],
      is_open: true,
      is_complete: false
    });
    Levels.create({
      type: 'student',
      title: 'Student',
      subtitle: '4 &times; 21',
      next_levels: ['programmer'],
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'programmer',
      title: 'Programmer',
      next_levels: ['engineer'],
      subtitle: '2 ^ 8',
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'engineer',
      title: 'Engineer',
      subtitle: '25 + 3 &times; 90',
      next_levels: ['professor', 'einstein'],
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'professor',
      title: 'Professor',
      subtitle: '2 ^ 3',
      is_open: false,
      is_complete: false
    });
    Levels.create({
      type: 'einstein',
      title: 'Einstein',
      subtitle: '9!',
      is_open: false,
      is_complete: false
    });

    var levels = ['schoolboy','student','programmer','engineer','professor','einstein'],
        def_timer = {'schoolboy': 10000, 'student': 6000, 'programmer': 6000, 'engineer': 10000, 'professor': 10000, 'einstein': 10000};
    // levels = ['schoolboy'];
    _.forEach(levels, function(level, L) {
      var i,j,open, timer, questions, timer_step, next_game, next_primary,
          next_end_game = ['all_complete', 0],
          max_i = 5,
          max_j = 4,
          min_time = 0.4;
      for (i=1; i<=max_i; i++)
      {
        timer = F(def_timer[level]);
        timer_step = F(( def_timer[level]*(1-min_time) ) / (max_j+1));
        questions = (i == 1) ? 6 : 12 ;

        if (i == max_i) {
          next_primary = (levels[L+1] ? [levels[L+1], '1'] : next_end_game)
        }
        else {
          next_primary = [level, (i+1).toString()]
        }

        open = (level === 'schoolboy' && i == 1);
        Games.create({
          level: level,
          sort: i*10,
          timer: timer,
          title: i.toString(),
          alias: i.toString(),
          questions: questions,
          next_game: [level, i+'.1'],
          next_primary: next_primary,
          awards: [F(timer*0.5), F(timer*0.65), F(timer*0.8)],
          is_open: open
        });
        for (j=1; j<=max_j; j++)
        {
          if (j == max_j && i == max_i && !levels[L+1]) {
            next_game = next_end_game;
          }
          else if (j == max_j && i == max_i) {
            next_game = [levels[L+1], '1'];
          }
          else if (j == max_j) {
            next_game = [level, (i+1)+'.1'];
          }
          else {
            next_game = [level, i+'.'+(j+1)];
          }

          timer = F(def_timer[level]-timer_step*j);
          Games.create({
            level: level,
            primary: i.toString(),
            sort: i*10+j,
            timer: timer,
            title: i+'.'+j,
            alias: i+'.'+j,
            questions: 6,
            next_game: next_game,
            awards: [F(timer*0.5), F(timer*0.65), F(timer*0.8)]
          });
        }
      }

      function F(value)
      {
        return parseFloat((value).toFixed(2));
      }
    })
  };
});
