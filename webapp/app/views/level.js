/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/levels',
  'collections/games',
  'collections/settings',
  'text!templates/level.html',
  'common'
], function ($, _, Backbone, LevelsCollection, GamesCollection, Settings, levelTemplate, Common) {
  'use strict';

  var LevelView = Backbone.View.extend({

    alias: 'level',
    el: '#level',
    level: null,

    template: _.template(levelTemplate),

    events: {},

    initialize: function(level)
    {
    },

    setLevel: function(value)
    {
      this.level = value;
    },

    render: function(level)
    {
      var level = LevelsCollection.getLevel(this.level),
          list = GamesCollection.getLevelGames(this.level);

      if (!level) {
        console.error('Game level not found');
        return;
      }

      this.updateBG();

      this.$el.html(this.template({
        title: level.get('title'),
        games: list
      }));
    },

    updateBG: function()
    {
      if (this.level == Settings.getKey('level')) {
        return;
      }
      Settings.setKey('level', this.level);
      $('body').attr('class', 'b-screen '+this.level);
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new LevelView();
});
