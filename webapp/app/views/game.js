/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/levels',
  'collections/games',
  'text!templates/game.html',
  'core/question_generator',
  'core/ad_mob',
  'common'
], function ($, _, Backbone, LevelsCollection, GamesCollection, gameTemplate, qGen, AdMob, Common) {
  'use strict';

  var errorTimer, startupTimer;

  var GameView = Backbone.View.extend({

    alias: 'game',
    el: '#game',
    game: null,
    game_id: null,
    level: null,

    question: 0,

    template: _.template(gameTemplate),

    events: {},

    initialize: function()
    {
    },

    setGameId: function(value)
    {
      this.game_id = value;
    },

    render: function()
    {
      var game, level;
      if (this.question !== 0) {
        return this.resume_game();
      }

      game = GamesCollection.get(this.game_id);
      if (!game) {
        console.error('Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      level = LevelsCollection.getLevel(game.get('level'));
      if (!level) {
        console.error('Game level not found');
        return;
      }

      if (!qGen(game.get('level'), game.get('alias'))) {
        console.error('qGen return null');
        Common.router.navigate('#/error500');
        return;
      }

      this.game = game;
      this.level = level;

      this.$el.html(this.template({
        game_id: game.get('id'),
        timer: game.get('timer'),
        level_title: level.get('title'),
        game_title: game.get('title'),
        questions: _.range(1, game.get('questions')+1)
      }));

      this.launch_game();

      // AdMob.prepareBanner();
    },

    pause_game: function()
    {
      clearTimeout(errorTimer);
      clearInterval(startupTimer);
    },

    resume_game: function() {},

    launch_game: function()
    {
      var self = this;
      var user_answer = '',
          rigth_answer, question_text,
          $keypad,
          $timer, $timer_startup,
          $answer, $answer_mask, $question, $question_block,
          $question_status;

      var question_pass_class = 'b-status__item_passed';

      $timer = $('#game-timer', this.el);
      $timer_startup = $('#game-timer-startup', this.el);
      $keypad = $('#game-keypad', this.el);
      $answer = $('#game-answer', this.el);
      $question = $('#game-question', this.el);
      $question_block = $('#game-question-block', this.el);
      $answer_mask = $('#game-answer-mask', this.el);
      $question_status = $('#game-question-status .js-q-stat', this.el);

      $keypad.on('click', 'a', function(e) {
        var button = e.currentTarget.dataset.button;
        if (button === 'backspace') {
          user_answer = user_answer.slice(0,-1);
        }
        else {
          user_answer += button;
        }

        $answer.text(user_answer);

        if (user_answer === rigth_answer) {
          set_right_answer();
        }
      });

      function next_question()
      {
        var q_data = qGen(self.game.get('level'), self.game.get('alias'));

        self.question += 1;
        user_answer = '';
        question_text = q_data[0];
        rigth_answer = q_data[1];

        $question.text(question_text);
        $answer.text(user_answer);
        $answer_mask.text(rigth_answer);

        $timer.addClass('ready-timer');
        $question_block.animate({opacity: 1}, 350, function(){
          $timer.addClass('start-timer');
          errorTimer = setTimeout(timeout_right_answer, self.game.get('timer'));
        });
      }

      function set_right_answer()
      {
        clearTimeout(errorTimer);
        $timer.removeClass('ready-timer start-timer');
        setTimeout(function(){
          $question_status.eq(self.question-1).addClass(question_pass_class);

          if (self.game.get('questions') === self.question) {
            return game_complete_success();
          }

          $question_block.animate({opacity: 0}, 350, next_question);
        }, 400);
      }

      function timeout_right_answer()
      {
        $keypad.off('click');
        game_complete_error();
      }

      function game_complete_success()
      {
        self.completeAndOpenNext();
        Common.router.navigate('#/game/'+self.game.get('id')+'/complete');
      }

      function game_complete_error()
      {
        Common.router.navigate('#/game/'+self.game.get('id')+'/error');
      }

      function start_game()
      {
        var timer = 3;
        $timer_startup.text(timer);
        startupTimer = setInterval(function(){
          timer -= 1;
          if (timer > 0) {
            return $timer_startup.text(timer);
          }

          clearInterval(startupTimer);
          $timer_startup.addClass('startup-timeout');
          next_question();
        }, 1000);
      }

      this.resume_game = start_game;
      this.pause_game = function() {
        clearTimeout(errorTimer);
        clearInterval(startupTimer);
        setTimeout(function() {
          self.question -= 1;
          $timer_startup.empty()
          $timer_startup.removeClass('startup-timeout');
          $timer.removeClass('ready-timer start-timer');
          $question_block.animate({opacity: 0}, 0);
        }, 350);
      }

      // start_game();
    },

    completeAndOpenNext: function()
    {
      var next_primary_game,
          next_game = GamesCollection.getNextGame(this.game);

      this.game.completeGame();

      if (next_game) {
        next_game.openGame();
      }

      if (GamesCollection.allGamesIsComplete(this.game.get('level'))) {
        this.level.completeLevel();
      }

      if (this.game.isPrimary()) {
        next_primary_game = GamesCollection.getNextGame(this.game, true);
        if (next_primary_game) {
          next_primary_game.openGame();
        }

        if (GamesCollection.allPrimaryIsComplete(this.game.get('level'))) {
          LevelsCollection.openNextLevels(this.level);
        }
      }
    },

    freeze: function()
    {
      clearTimeout(errorTimer);
      clearInterval(startupTimer);
      this.pause_game();
    },

    destroy: function()
    {
      this.question = 0;
      this.$el.empty()
    }

  });

  return new GameView();
});
