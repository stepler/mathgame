/*global require*/
'use strict';

// Require.js allows us to configure shortcut alias
require.config({
  map: {
    '*': {
      'underscore': 'lodash',
      'jquery': 'zepto'
    }
  },
  shim: {
    'zepto': {
      exports: '$'
    },
    lodash: {
      exports: '_'
    },
    backbone: {
      deps: [
        'lodash',
        'zepto'
      ],
      exports: 'Backbone'
    },
    backboneLocalstorage: {
      deps: ['backbone'],
      exports: 'Store'
    }
  },
  paths: {
    zepto: '../libs/zepto.min',
    lodash: '../libs/lodash.min',
    backbone: '../libs/backbone/backbone',
    backboneLocalstorage: '../libs/backbone/localstorage',
    text: '../libs/requirejs/text'
  }
});

require([
  'backbone',
  'core/bootstrap',
  'core/router',
  'common'
], function (Backbone, Bootstrap, Router, Common) {

  Bootstrap(function(){
    var router = new Router();
    Backbone.history.start();
    Common.router = router;
  });
});
