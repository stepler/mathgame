/*global define*/
define([
  'zepto',
  'lodash'
], function ($, _) {
  'use strict';

  var default_options = {},
      admobid = {},
      is_enable = !!window.AdMob,
      is_prepare = false;

  if (is_enable)
  {
    if (/(android)/i.test(navigator.userAgent) ) {
      admobid = { // for Android
        banner: 'ca-app-pub-6869992474017983/9375997553',
        interstitial: 'ca-app-pub-6869992474017983/1657046752'
      };
    } else if(/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
      admobid = { // for iOS
        banner: 'ca-app-pub-6869992474017983/4806197152',
        interstitial: 'ca-app-pub-6869992474017983/7563979554'
      };
    }

    var default_options = {
        bannerId: admobid.banner,
        interstitialId: admobid.interstitial,
        adSize: 'SMART_BANNER',
        position: AdMob.AD_POSITION.TOP_CENTER,
        // offsetTopBar: false, // avoid overlapped by status bar, for iOS7+
        bgColor: 'black', // color name, or '#RRGGBB'
        isTesting: true,
        autoShow: false
    };
    AdMob.setOptions(default_options);
  }

  return {
    prepareBanner: function()
    {
      if (is_enable && !is_prepare) {
        is_prepare = true;
        AdMob.createBanner({autoShow: false});
      }
    },
    showBanner: function()
    {
      if (is_enable && is_prepare) {
        is_prepare = false;
        AdMob.showBanner();
      }
    }
  }
});
