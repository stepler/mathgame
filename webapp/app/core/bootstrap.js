/*global define*/
define([
  'zepto',
  'lodash',
  'core/startup',
  'collections/settings',
  'collections/levels',
  'collections/games',
  'views/main',
  'views/level',
  'views/game',
  'views/game_complete',
  'views/game_pause',
  'views/error500',
  'common'
], function ($, _, Startup, Settings, LevelsCollection, GamesCollection, MV, LV, GV, GCV, GPV, EV, Common) {
  'use strict';

  function init_startup()
  {
    var level;

    Settings.fetch({reset:true});

    level = Settings.getKey('level');
    $('body').attr('class', 'b-screen '+level);

    if (level) {
      return;
    }

    Startup();
    Settings.setKey('level', 'schoolboy');
  }

  function init_collections()
  {
    LevelsCollection.fetch({reset:true});
    GamesCollection.fetch({reset:true});
  }

  function init_views()
  {
    // Вьюхи проинициализировались при загрузке =)
  }

  init_startup();
  init_collections();
  init_views();

  return function(callback) {
    callback();
  };
});
