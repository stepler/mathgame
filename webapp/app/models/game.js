/*global define*/
define([
  'lodash',
  'backbone'
], function (_, Backbone) {
  'use strict';

  var Game = Backbone.Model.extend({

    defaults: {
      level: '',
      primary: 0,
      sort: 0,
      alias: '',
      title: '',
      score: 0,
      questions: 12,
      timer: 0,
      awards: [],
      next_game: [],
      next_primary: [],
      is_open: false,
      is_complete: false
    },

    openGame: function()
    {
      this.save({
        is_open: true
      });
    },

    completeGame: function () {
      this.save({
        is_complete: true
      });
    },

    isPrimary: function () {
      return this.get('primary') === 0;
    }
  });

  return Game;
});
