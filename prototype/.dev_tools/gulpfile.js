// Core modules
var path = require('path');

// Gulp modules
var gulp = require('gulp'),
    watch = require('gulp-watch'),
    gulpif = require('gulp-if'),
    less = require('gulp-less'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    argv = require('yargs').argv,
    runSequence = require('run-sequence'),
    WebSocketServer = new require('ws');

// Variables
var STATIC_DIR = path.join(__dirname, '..', 'static'),
    CSS_DIR = STATIC_DIR+'/css',
    BEM_DIR = STATIC_DIR+'/bem-blocks',
    BEM_RELDIR = BEM_DIR.replace(/\.\.?\//, '/');


/** 
 * Работаем с стилями
 */
gulp.task('style-less', function() {
  return gulp.src(BEM_DIR+'/bem.less')
    .pipe(less())
    .pipe(rename({basename: 'style'}))
    .pipe(gulp.dest(CSS_DIR))
    .on('end', function(){ 
      WS.send({type: 'css', source: '/static/css/style.css'});
    });
});

gulp.task('style-watch', function() {
  gulp.watch(BEM_DIR+'/**/*.less', ['style-less']);
  WS.init();
});

gulp.task('style-min', function() {
  return gulp.src(CSS_DIR+'/style.css')
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(CSS_DIR));
});

/**
 * Работаем с JS
 * TODO: Сборка и минификация bem скриптов
 *       Сборка и минификация не-bem скриптов
 *       Сборка и минификация подключаемых библиотек (в идеале с разбивкой по размеру)
 */


/** 
 * Собираем проект
 */
gulp.task('build-style', function(cb) {
  runSequence('style-less', 'style-min');
});

gulp.task('build', function() {
  runSequence('build-style')
});


/**
 * WS сервер для WebpageLiveUpdate chrome-плагина
 * INFO: http://git.webpp.ru/tools/project-stub/wikis/WebpageLiveUpdate
 */
var WS = {
  server: null,
  pool: {},
  init: function()
  {
    var self = this;
    this.server = new WebSocketServer.Server({host:'0.0.0.0', port: 35729});
    this.server.on('connection', function(ws) 
    {
      var id = Math.random();
      self.pool[id] = ws;
      // console.log('New connection ' + id);

      ws.on('close', function() {
        // console.log('Connection close ' + id);
        delete self.pool[id];
      });
    });

    // Хак, для поддержки соединения.
    // TODO: сделать нормальное решение.
    setInterval(function(){
      self.send({type: 'ping'});
    }, 5000);
  },
  send: function(data)
  {
    var message = JSON.stringify(data);  
    for(var id in this.pool) {
      this.pool[id].send(message);
    }
  }
};