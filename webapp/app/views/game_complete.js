/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'collections/games',
  'text!templates/game-complete.html',
  'core/ad_mob',
  'common'
], function ($, _, Backbone, GamesCollection, gameCompleteTemplate, AdMob, Common) {
  'use strict';

  var GameComplete = Backbone.View.extend({

    alias: 'game_complete',
    el: '#game_complete',
    game_id: null,
    game_status: null,

    template: _.template(gameCompleteTemplate),

    events: {},

    initialize: function(level)
    {
    },

    setGameId: function(value)
    {
      this.game_id = value;
    },

    setGameStatus: function(value)
    {
      this.game_status = value;
    },

    render: function(level)
    {
      var next_game,
          game = GamesCollection.get(this.game_id);

      if (!game) {
        console.error('Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      next_game = game;
      if (this.game_status) {
        next_game = GamesCollection.getNextGame(game, game.isPrimary());
      }

      if (!next_game) {
        if (GamesCollection.nextGameIsEnd(game)) {
          Common.router.navigate('#/game/end');
          return;
        }
        console.error('Next Game not found');
        Common.router.navigate('#/error500');
        return;
      }

      this.$el.html(this.template({
        is_complete: this.game_status,
        next_game_id: next_game.get('id'),
        description: ''
      }));

      AdMob.showBanner();
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new GameComplete();
});
