/*global define */
define([
  'lodash',
  'backbone',
  'backboneLocalstorage',
  'models/setting'
], function (_, Backbone, Store, Setting) {
  'use strict';

  var Settings = Backbone.Collection.extend({

    model: Setting,

    localStorage: new Store('game_settings'),

    getKey: function (key) {
      var item = this.findWhere({key: key});
      return item ? item.get('value') : null ;
    },

    setKey: function (key, value) {
      var item = this.findWhere({key: key});
      if (item) {
        item.save({value: value});
      }
      else {
        this.create({key: key, value: value});
      }
    }
  });

  return new Settings();
});
