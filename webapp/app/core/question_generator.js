/*global define*/
define([
  'lodash'
], function (_) {
  'use strict';

  var generator = {
    schoolboy: {
      '1': function() {
        var f = _.random(1, 3),
            s = _.random(1, 3);
        return [(f+' + '+s), (f+s).toString()];
      },
      '2': function() {
        var f = _.random(1, 5),
            s = _.random(1, 5);
        return [(f+' + '+s), (f+s).toString()];
      },
      '3': function() {
        return this['2']();
      },
      '4': function() {
        return this['2']();
      },
      '5': function() {
        return this['2']();
      }
    },
    student: {
      '1': function() {
        var f = _.random(1, 10),
            s = _.random(1, 10);
        return [(f+' + '+s), (f+s).toString()];
      },
    }
  };

  return function(level, game) {
    var game = (game.split('.'))[0];

    if (generator[level] && generator[level][game]) {
      return generator[level][game]();
    }
    return null;
  }
});
