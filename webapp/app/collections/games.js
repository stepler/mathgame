/*global define */
define([
  'lodash',
  'backbone',
  'backboneLocalstorage',
  'models/game'
], function (_, Backbone, Store, Game) {
  'use strict';

  var Games = Backbone.Collection.extend({

    model: Game,
    comparator: 'sort',

    localStorage: new Store('game.games'),

    getLevelGames: function(level)
    {
      var i,
          list = _.map(this.where({level: level, primary: 0}), function(model){ return model.toJSON(); });

      if (!list.length) {
        return [];
      }
      for (i in list) {
        list[i].subgames = _.map(this.where({level: level, primary: list[i].alias}), function(model){ return model.toJSON(); });
      }
      return list;
    },

    getNextGame: function(game, primary)
    {
      var next_game = game.get(primary?'next_primary':'next_game');
      return this.findWhere({level: next_game[0], alias: next_game[1]});
    },

    nextGameIsEnd: function(game)
    {
      var next_game = game.get('next_game');
      return this.findWhere({level: next_game[0], alias: next_game[1]});
    },

    allPrimaryIsComplete: function(level)
    {
      var list = this.where({level: level, primary: 0, is_complete: false});
      return list.length === 0;
    },

    allGamesIsComplete: function(level)
    {
      var list = this.where({level: level, is_complete: false});
      return list.length === 0;
    }
  });

  return new Games();
});
