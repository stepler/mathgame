/*global define*/
define([
  'zepto',
  'lodash',
  'backbone',
  'text!templates/error500.html',
  'common'
], function ($, _, Backbone, error500Template, Common) {
  'use strict';

  var Error500View = Backbone.View.extend({

    alias: 'error500',
    el: '#error500',

    template: _.template(error500Template),

    events: {},

    initialize: function(level)
    {
    },

    render: function(level)
    {
      this.$el.html(this.template());
    },

    freeze: function() {},

    destroy: function()
    {
      this.$el.empty()
    }
  });

  return new Error500View();
});
